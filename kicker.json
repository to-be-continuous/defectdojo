{
  "name": "DefectDojo",
  "description": "Import security reports into [DefectDojo](https://www.defectdojo.org/)",
  "template_path": "templates/gitlab-ci-defectdojo.yml",
  "kind": "analyse",
  "prefix": "defectdojo",
  "is_component": true,
  "variables": [
    {
      "name": "DEFECTDOJO_BASE_IMAGE",
      "description": "The Docker image used to run import",
      "default": "registry.hub.docker.com/library/node:alpine3.11"
    },
    {
      "name": "DEFECTDOJO_SERVER_URL",
      "description": "URL of DefectDojo server",
      "mandatory": true
    },
    {
      "name": "DEFECTDOJO_API_KEY",
      "description": "Your DefectDojo API v2 key - must be saved as a masked CI/CD variable",
      "secret": true,
      "mandatory": true
    },
    {
      "name": "DEFECTDOJO_DIR",
      "description": "DefectDojo working directory",
      "default": ".",
      "advanced": true
    },
    {
      "name": "DEFECTDOJO_NOPROD_ENABLED",
      "description": "Determines whether security reports produced in non-production branches are uploaded to DefectDojo",
      "type": "boolean"
    },
    {
      "name": "DEFECTDOJO_TIMEZONE",
      "description": "Time zone used for naming imports in DefectDojo",
      "default": "Europe/Paris",
      "advanced": true
    },
    {
      "name": "DEFECTDOJO_SMTP_SERVER",
      "description": "name:port of SMTP server used for notifications - if this value is set, DefectDojo will send an e-mail notification in case of new vulnerabilities",
      "advanced": true
    },
    {
      "name": "DEFECTDOJO_NOTIFICATION_SEVERITIES",
      "description": "List of severities for which you want to be notified - DEFECTDOJO_SMTP_SERVER must be defined if you want to be notified",
      "default": "Critical,High",
      "advanced": true
    },
    {
      "name": "DEFECTDOJO_BANDIT_REPORTS",
      "description": "path to Bandit JSON reports",
      "default": "bandit*.json reports/py-bandit.bandit.json",
      "advanced": true
    },
    {
      "name": "DEFECTDOJO_DC_REPORTS",
      "description": "Path to Dependency Check reports",
      "default": "dependency-check*.xml",
      "advanced": true
    },
    {
      "name": "DEFECTDOJO_DC_GRADLE_REPORTS",
      "description": "Path to Dependency Check reports from Gradle template",
      "default": "dependency-check*.xml",
      "advanced": true
    },
    {
      "name": "DEFECTDOJO_GITLEAKS_REPORTS",
      "description": "Path to Gitleaks reports",
      "default": "gitleaks/gitleaks-report.json reports/gitleaks.native.json",
      "advanced": true
    },
    {
      "name": "DEFECTDOJO_HADOLINT_REPORTS",
      "description": "Path to Hadolint reports",
      "default": "hadolint-json-*.json reports/docker-hadolint-*.native.json",
      "advanced": true
    },
    {
      "name": "DEFECTDOJO_MOBSF_REPORTS",
      "description": "Path to MobSF reports",
      "default": "mobsf*.json",
      "advanced": true
    },
    {
      "name": "DEFECTDOJO_NODEJSSCAN_REPORTS",
      "description": "Path to NodeJSScan reports",
      "default": "nodejsscan-report-sarif.json",
      "advanced": true
    },
    {
      "name": "DEFECTDOJO_NPMAUDIT_REPORTS",
      "description": "Path to NPMAudit reports",
      "default": "npm-audit*.json",
      "advanced": true
    },
    {
      "name": "DEFECTDOJO_TESTSSL_REPORTS",
      "description": "Path to TestSSL reports",
      "default": "reports/testssl.native.csv",
      "advanced": true
    },
    {
      "name": "DEFECTDOJO_TRIVY_REPORTS",
      "description": "Path to Trivy reports",
      "default": "trivy/*.json trivy-*.json reports/docker-trivy-*.native.json reports/py-trivy.trivy.json",
      "advanced": true
    },
    {
      "name": "DEFECTDOJO_ZAP_REPORTS",
      "description": "Path to Zap reports",
      "default": "reports/zap.native.xml",
      "advanced": true
    },
    {
      "name": "ZAP_TPL_PROJECT",
      "description": "Path to Zap template",
      "advanced": true
    },
    {
      "name": "DEFECTDOJO_SEMGREP_REPORTS",
      "description": "Path to Semgrep reports",
      "default": "reports/semgrep.native.json",
      "advanced": true
    },
    {
      "name": "SEMGREP_TEMPLATE",
      "description": "Path to Semgrep template",
      "advanced": true
    },    
    {
      "name": "DEFECTDOJO_SONARQUBE_SINCELEAKPERIOD",
      "description": "Determines if delta analysis is activated for SonarQube export",
      "type": "boolean",
      "advanced": true
    },
    {
      "name": "DEFECTDOJO_SONARQUBE_NOSECURITYHOTSPOT",
      "description": "Set this flag to true if SonarQube version does not support security hotspots (v < 7.3)",
      "type": "boolean",
      "default": "true",
      "advanced": true
    },
    {
      "name": "DEFECTDOJO_SONARQUBE_ALLBUGS",
      "description": "In SonarQube, determines if all bugs are exported (true) or only vulnerabilities (false)",
      "type": "boolean",
      "advanced": true
    }
  ],
  "variants": [
    {
      "id": "vault",
      "name": "Vault",
      "description": "Retrieve secrets from a [Vault](https://www.vaultproject.io/) server",
      "template_path": "templates/gitlab-ci-defectdojo-vault.yml",
      "variables": [
        {
          "name": "TBC_VAULT_IMAGE",
          "description": "The [Vault Secrets Provider](https://gitlab.com/to-be-continuous/tools/vault-secrets-provider) image to use",
          "default": "registry.gitlab.com/to-be-continuous/tools/vault-secrets-provider:latest",
          "advanced": true
        },
        {
          "name": "VAULT_BASE_URL",
          "description": "The Vault server base API url",
          "mandatory": true
        },
        {
          "name": "VAULT_OIDC_AUD",
          "description": "The `aud` claim for the JWT",
          "default": "$CI_SERVER_URL"
        },
        {
          "name": "VAULT_ROLE_ID",
          "description": "The [AppRole](https://www.vaultproject.io/docs/auth/approle) RoleID",
          "mandatory": true,
          "secret": true
        },
        {
          "name": "VAULT_SECRET_ID",
          "description": "The [AppRole](https://www.vaultproject.io/docs/auth/approle) SecretID",
          "mandatory": true,
          "secret": true
        }
      ]
    }
  ]
}
